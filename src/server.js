import express from "express";
import ExampleController from "./app/example/controller";

const app = express();

export const setup = () => { 
    app.get("/", (req, res) => res.send({ test: "Hello World!" }));

    app.get("/example", (req, res) => ExampleController.getExample(req, res));
    app.listen(3000, () => console.log("running listen port 3000!"));
}