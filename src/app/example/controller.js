class ExampleController {
  static getExample(req, res) {
    res.send([
      {
        example: 1,
        desc: "test example",
      },
      {
        example: 2,
        desc: "test example",
      },
    ]);
  }
}
export default ExampleController;
